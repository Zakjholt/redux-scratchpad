export type ActionType<T, AType> = T extends (...args: any[]) => infer R
  ? R extends { type: string } ? R & { type: AType } : R
  : any;
