import { Reducer } from 'redux';
import { NotifActions, NOTIF } from '../actions/notif';

const notif: Reducer<{}> = (state = {}, action: NotifActions) => {
  switch (action.type) {
    case NOTIF.PUSH: {
      return state;
    }
    case NOTIF.DISMISS: {
      return state;
    }
    default:
      return state;
  }
};

export default notif;
