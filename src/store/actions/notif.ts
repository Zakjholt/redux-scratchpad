import { ActionType } from '../util';

export const enum NOTIF {
  PUSH = 'NOTIF_PUSH',
  DISMISS = 'NOTIF_DISMISS'
}

type NotificationIntent = 'success' | 'info' | 'error';
const pushNotification = (intent: NotificationIntent, message: string) => ({
  type: NOTIF.PUSH,
  intent,
  message
});

const dismissNotification = (id: string) => ({
  type: NOTIF.DISMISS,
  id
});

// perhaps make this into a helper that can collect exports
export type NotifActions =
  | ActionType<typeof pushNotification, NOTIF.PUSH>
  | ActionType<typeof dismissNotification, NOTIF.DISMISS>;

export { pushNotification, dismissNotification };
